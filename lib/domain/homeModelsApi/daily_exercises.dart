import 'package:json_annotation/json_annotation.dart';
part 'daily_exercises.g.dart';

@JsonSerializable()
class DailyExercises {
  final int id;
  final String title;
  final String image;
  final int duration;
  final int repetitions;
  @JsonKey(name: 'burn_calories')
  final int burnCalories;

  DailyExercises({
    required this.id,
    required this.title,
    required this.image,
    required this.duration,
    required this.repetitions,
    required this.burnCalories
  });

  factory DailyExercises.fromJson(Map<String, dynamic> json) => _$DailyExercisesFromJson(json);
  Map<String, dynamic> toJson() => _$DailyExercisesToJson(this);
}