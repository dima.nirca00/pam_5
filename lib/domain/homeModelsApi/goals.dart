import 'package:json_annotation/json_annotation.dart';

part 'goals.g.dart';

@JsonSerializable()
class Goals {
  final String image;
  final String title;
  final String description;
  final int duration;
  @JsonKey(name: 'burn_calories')
  final int burnCalories;

  Goals({
    required this.image,
    required this.title,
    required this.description,
    required this.duration,
    required this.burnCalories
  });

  factory Goals.fromJson(Map<String, dynamic> json)=> _$GoalsFromJson(json);
  Map<String, dynamic> toJson() => _$GoalsToJson(this);
}