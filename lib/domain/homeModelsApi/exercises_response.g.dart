// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'exercises_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExercisesResponse _$ExercisesResponseFromJson(Map<String, dynamic> json) =>
    ExercisesResponse(
      (json['results'] as List<dynamic>)
          .map((e) => DailyExercises.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ExercisesResponseToJson(ExercisesResponse instance) =>
    <String, dynamic>{
      'results': instance.exercises,
    };
