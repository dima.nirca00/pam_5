// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'daily_exercises.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DailyExercises _$DailyExercisesFromJson(Map<String, dynamic> json) =>
    DailyExercises(
      id: json['id'] as int,
      title: json['title'] as String,
      image: json['image'] as String,
      duration: json['duration'] as int,
      repetitions: json['repetitions'] as int,
      burnCalories: json['burn_calories'] as int,
    );

Map<String, dynamic> _$DailyExercisesToJson(DailyExercises instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'image': instance.image,
      'duration': instance.duration,
      'repetitions': instance.repetitions,
      'burn_calories': instance.burnCalories,
    };
