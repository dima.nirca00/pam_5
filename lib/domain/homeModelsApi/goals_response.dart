import 'package:json_annotation/json_annotation.dart';
import 'goals.dart';
part 'goals_response.g.dart';


@JsonSerializable()
class GoalsResponse {
  @JsonKey(name: 'results')
  final List<Goals> goals;

  GoalsResponse( this.goals);
  factory GoalsResponse.fromJson(Map<String, dynamic> json) => _$GoalsResponseFromJson(json);
  Map<String, dynamic> toJson() => _$GoalsResponseToJson(this);
}