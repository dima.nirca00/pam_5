import 'package:json_annotation/json_annotation.dart';
import 'daily_exercises.dart';

part 'exercises_response.g.dart';


@JsonSerializable()
class ExercisesResponse {
  @JsonKey(name: 'results')
  final List<DailyExercises> exercises;

  ExercisesResponse(this.exercises);
  factory ExercisesResponse.fromJson(Map<String, dynamic> json) => _$ExercisesResponseFromJson(json);
  Map<String, dynamic> toJson() => _$ExercisesResponseToJson(this);
}