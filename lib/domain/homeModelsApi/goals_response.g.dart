// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'goals_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GoalsResponse _$GoalsResponseFromJson(Map<String, dynamic> json) =>
    GoalsResponse(
      (json['results'] as List<dynamic>)
          .map((e) => Goals.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GoalsResponseToJson(GoalsResponse instance) =>
    <String, dynamic>{
      'results': instance.goals,
    };
