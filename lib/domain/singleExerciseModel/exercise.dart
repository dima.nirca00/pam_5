import 'package:json_annotation/json_annotation.dart';

import 'next_exercise.dart';

part 'exercise.g.dart';

@JsonSerializable()
class SingleExercise {
  final int id;
  final String image;
  final String title;
  final int repetitions;
  final int duration;
  @JsonKey(name: 'burn_calories')
  final int burnCalories;
  final int total;
  @JsonKey(name: 'next_exercise')
  final NextExercise nextExercise;

  SingleExercise({
    required this.id,
    required this.image,
    required this.title,
    required this.repetitions,
    required this.duration,
    required this.burnCalories,
    required this.total,
    required this.nextExercise
  });

  factory SingleExercise.fromJson(Map<String, dynamic> json) => _$SingleExerciseFromJson(json);
  Map<String, dynamic> toJson() => _$SingleExerciseToJson(this);
}