// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'next_exercise.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NextExercise _$NextExerciseFromJson(Map<String, dynamic> json) => NextExercise(
      id: json['id'] as int,
      title: json['title'] as String,
    );

Map<String, dynamic> _$NextExerciseToJson(NextExercise instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
    };
