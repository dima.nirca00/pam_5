// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'exercise.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SingleExercise _$SingleExerciseFromJson(Map<String, dynamic> json) =>
    SingleExercise(
      id: json['id'] as int,
      image: json['image'] as String,
      title: json['title'] as String,
      repetitions: json['repetitions'] as int,
      duration: json['duration'] as int,
      burnCalories: json['burn_calories'] as int,
      total: json['total'] as int,
      nextExercise:
          NextExercise.fromJson(json['next_exercise'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$SingleExerciseToJson(SingleExercise instance) =>
    <String, dynamic>{
      'id': instance.id,
      'image': instance.image,
      'title': instance.title,
      'repetitions': instance.repetitions,
      'duration': instance.duration,
      'burn_calories': instance.burnCalories,
      'total': instance.total,
      'next_exercise': instance.nextExercise,
    };
