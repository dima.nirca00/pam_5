import 'package:json_annotation/json_annotation.dart';

part 'next_exercise.g.dart';

@JsonSerializable()
class NextExercise {
  final int id;
  final String title;

  NextExercise({
    required this.id,
    required this.title,
  });

  factory NextExercise.fromJson(Map<String, dynamic> json) => _$NextExerciseFromJson(json);
  Map<String, dynamic> toJson() => _$NextExerciseToJson(this);
}