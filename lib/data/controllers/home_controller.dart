import 'dart:convert';
import 'package:get/get.dart';
import '../../domain/homeModelsApi/daily_exercises.dart';
import 'package:http/http.dart' as http;
import '../../domain/homeModelsApi/exercises_response.dart';
import '../../domain/homeModelsApi/goals.dart';
import '../../domain/homeModelsApi/goals_response.dart';



class HomeController extends GetxController{
  RxList<Goals> goalsList = RxList();
  RxList<DailyExercises> exercisesList = RxList();


  void readGoalsApiData() async {
    final response = await http.get(Uri.parse('https://fitness-app-api.k8s.devebs.net/workout/goal'));
    if (response.statusCode == 200) {
      var map = await jsonDecode(response.body);
      var mapResponse = GoalsResponse.fromJson(map);
      goalsList.value = mapResponse.goals
          .map((e) => Goals(
          image: e.image,
          title: e.title,
          description: e.description,
          duration: e.duration,
          burnCalories: e.burnCalories))
          .toList();
    } else {
      throw Exception('Failed to load goals');
    }
  }

  void readExerciseApiDataPaged(int page, int size) async {
    // final response = await http.get(Uri.parse('https://mocki.io/v1/17ddd338-72a0-4a55-9615-82af5d02bc52'));
    final response = await http.get(Uri.parse('https://fitness-app-api.k8s.devebs.net/workout/exercise?page=' + page.toString() + '&size=7'));
    if (response.statusCode == 200) {
      var map = await jsonDecode(response.body);
      var mapResponse = ExercisesResponse.fromJson(map);
      exercisesList.value = mapResponse.exercises
          .map((e) => DailyExercises(
        id: e.id,
        title: e.title,
        image: e.image,
        duration: e.duration,
        repetitions: e.repetitions,
        burnCalories: e.burnCalories,
      )).toList();
    } else {
      throw Exception('Failed to load exercises');
    }
  }

}