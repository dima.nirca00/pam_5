import 'dart:convert';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import '../../domain/singleExerciseModel/exercise.dart';

class ExerciseController extends GetxController{
  Rxn<SingleExercise> exercise = Rxn();
  late SingleExercise larisa;
  void readExerciseApiData(int id) async {
    final responseExercise1 = await http.get(Uri.parse('https://fitness-app-api.k8s.devebs.net/workout/exercise/' + id.toString()));
    // print(responseExercise1 'aolei');
    if (responseExercise1.statusCode == 200) {
      var map = await jsonDecode(responseExercise1.body);
      print(map);
      larisa = SingleExercise.fromJson(map);
      print(larisa);
      exercise.value = larisa;
    } else {
      throw Exception('Failed to load goals');
    }

  }
}