import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lab4fitness/presentation/single_exercise_page/single_exerciseDesign.dart';
import '../../data/controllers/single_exercise_controller.dart';


class ExerciseSection2 extends StatefulWidget {
   int numberEx;
   int total;

  ExerciseSection2({super.key, required this.numberEx, required this.total});

  @override
  _ExerciseSection createState() => _ExerciseSection(this.numberEx, this.total);
}

class _ExerciseSection extends State<ExerciseSection2> with TickerProviderStateMixin{
   int numberEx;
   int total;

  _ExerciseSection(this.numberEx,this.total);


   @override
   void initState() {
     Get.lazyPut(() =>  ExerciseController());
     ExerciseController controllerSingleExercise = Get.find();
     controllerSingleExercise.readExerciseApiData(numberEx);
     super.initState();
   }
   void dispose(){
     super.dispose();
   }

  @override
  Widget build(BuildContext context) {
    ExerciseController controllerSingleExercise = Get.find();
    return Scaffold(
        body:  Obx( () => controllerSingleExercise.exercise.value != null
            ? SingleExerciseDesign(numberEx: numberEx, total: total, exercise: controllerSingleExercise.exercise.value!)
            : const Center(child: CircularProgressIndicator())
        )
    );

  }

}