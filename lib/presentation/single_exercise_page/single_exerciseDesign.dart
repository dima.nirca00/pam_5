import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:lab4fitness/presentation/single_exercise_page/single_exercise2.dart';
import '../../data/controllers/home_controller.dart';
import '../../data/controllers/single_exercise_controller.dart';
import '../../domain/singleExerciseModel/exercise.dart';
import '../home/home.dart';

class SingleExerciseDesign extends StatefulWidget {

  int numberEx;
  int total;
  SingleExercise exercise;
  SingleExerciseDesign({super.key,required this.numberEx, required this.total,required this.exercise});
  @override
  _SingleExerciseDesign createState() => _SingleExerciseDesign(this.numberEx, this.total,this.exercise);
}

class _SingleExerciseDesign extends State<SingleExerciseDesign> with TickerProviderStateMixin{
  SingleExercise exercise;
  int numberEx;
  int total;
  _SingleExerciseDesign(this.numberEx, this.total,this.exercise);
  bool isPlaying = false;
  final homeController = Get.put(HomeController());


  double progress = 1.0;
  String nextexercise = '';
  late AnimationController controller;
  Duration? timer;

  String get countText {
    Duration count = controller.duration! * controller.value;
    return controller.isDismissed
        ? '${(controller.duration!.inMinutes % 60).toString().padLeft(2, '0')}:${(controller.duration!.inSeconds % 60).toString().padLeft(2, '0')}'
        : '${(count.inMinutes % 60).toString().padLeft(2, '0')}:${(count.inSeconds % 60 + 1).toString().padLeft(2, '0')}';
  }

  void notify() {
    if (countText == '00:01') {
      Timer(Duration(milliseconds: 1100), () {

        ExerciseController controllerSingleExercise = Get.find();
        controllerSingleExercise.readExerciseApiData(numberEx+1);
        exercise = controllerSingleExercise.exercise.value!;
        isPlaying = false;
        controller.duration = Duration(seconds: exercise.duration);
        initState();
      });
    }
  }
  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
  @override
  void initState() {
    super.initState();


    controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: exercise.duration),
    );

    controller.addListener(() {
      notify();
      if (controller.isAnimating) {
        setState(() {
          progress = controller.value;
        });
      } else {
        setState(() {
          progress = 1.0;
        });
      }
    });
    /*controller.reverse(
        from: controller.value == 0 ? 1.0 : controller.value);
    setState(() {

    });*/
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(0.0), // here the desired height
            child: AppBar()),
        body: Column(
            mainAxisAlignment : MainAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 0,
              ),
              Row(
                  children: <Widget>[
                    GestureDetector(
                      child: Container(
                        margin: const EdgeInsets.only(left: 16.0, right: 81),
                        height: 48,
                        width: 48,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(24),
                          color: Colors.white,
                          border: Border.all(
                              color: const Color.fromRGBO(239, 239, 239, 1),
                              width: 2),
                        ),
                        child: Container(
                          //  left: 0,
                          //  bottom: 0,
                            child: SvgPicture.asset(
                                'resources/svg/left.svg',
                                height: 10,
                                width: 10,
                                fit: BoxFit.scaleDown)
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => HomePage(),
                          ),
                        );},
                    ),
                    Text("Exercise " + this.numberEx.toString() + "/" + this.total.toString(),
                        style: const TextStyle(
                            color: Color.fromRGBO(0, 19, 44, 1),
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.bold,
                            fontSize: 18))
                  ]
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 32),
                height: 272,
                width: 372,
                decoration:  BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(exercise.image.toString())),
                  color: Colors.redAccent,
                ),
              ),
              isPlaying ?
              Text(exercise.title.toString(),
                  style: const TextStyle(
                      color: Color.fromRGBO(0, 19, 44, 1),
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.bold,
                      fontSize: 28)) :
                  const Text("Ready To Go!",
                      style: TextStyle(
                      color: Color.fromRGBO(25, 143, 179, 1),
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.bold,
                      fontSize: 28))
              ,
              Container(
                  margin: EdgeInsets.symmetric(vertical: 16),
                  child:
                  isPlaying ?
                   AnimatedBuilder(
                    animation: controller,
                    builder: (context, child) => Text(
                      countText,
                      style: const TextStyle(
                        fontSize: 24,
                        color: Color.fromRGBO(25, 143, 179, 1),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ):
                  Text(exercise.title.toString(),
                      style: const TextStyle(
                          color: Color.fromRGBO(0, 19, 44, 1),
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 18))
              ),
              Container(
                margin: const EdgeInsets.only(left: 65.0),
                child:Row(
                  children: <Widget>[
                    Container(
                        height: 50,
                        width: 50,
                        /*child: this.numberEx == 1 ?
                        Container(
                          height: 40,
                          width: 40,) :
                        GestureDetector(
                          child: Stack(children: [
                             Positioned(
                                left: 0,
                                bottom: 0,
                                child: SvgPicture.asset('resources/svg/left.svg',
                                    height: 30, width: 30, fit: BoxFit.scaleDown))
                          ],)
                          ,
                          onTap: (){
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => ExerciseSection2(numberEx: numberEx-1, total: total),
                              ),
                            );
                          },
                        )*/
                    ),
                    Container(
                      child: Stack(
                        // alignment: Alignment.center,
                        children: [
                          Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 9),
                              child:
                              SizedBox(
                                width: 166,
                                height: 166,
                                child: CircularProgressIndicator(
                                  backgroundColor: Color.fromRGBO(245, 245, 245, 1),
                                  value: progress,
                                  strokeWidth: 15,
                                  color: const Color.fromRGBO(25, 143, 179, 1),
                                ),
                              )),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 54, vertical: 43),

                            child: GestureDetector(
                              onTap: () {
                              },
                              child: ElevatedButton(
                                child: SvgPicture.asset( isPlaying == true ? 'resources/svg/start_alb.svg' : 'resources/svg/pause.svg'),
                                onPressed: () {
                                  if (controller.isAnimating) {
                                    controller.stop();
                                    setState(() {
                                      isPlaying = false;
                                    });
                                  } else {
                                    controller.reverse(
                                        from: controller.value == 0 ? 1.0 : controller.value);
                                    setState(() {
                                      isPlaying = true;
                                    });
                                  }},
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Color.fromRGBO(25, 143, 179, 1),
                                  fixedSize: const Size(76, 76),
                                  shape: const CircleBorder(),
                                ),
                              ),
                            )
                          )
                        ],
                      ),
                    ),
                   /* Container(
                        height: 40,
                        width: 40,
                        child: this.numberEx == this.total ?
                        Container(
                          height: 40,
                          width: 40,) :
                        GestureDetector(
                          child: Stack(
                            children: <Widget> [
                              Positioned(
                                  left: 0,
                                  bottom: 0,
                                  child: SvgPicture.asset('resources/svg/right.svg',
                                      height: 30, width: 30, fit: BoxFit.scaleDown))
                            ],
                          )
                         ,
                          onTap: (){
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) =>
                                    ExerciseSection2(numberEx: numberEx+1, total: total),
                              ),
                            );
                          },
                        )
                    ),*/
                  ],
                ),
              ),
              Container(
                  margin: const EdgeInsets.only(top: 40.0),
                  child: const Text("Next",
                      style: const TextStyle(
                          color: Color.fromRGBO(148, 158, 159, 1),
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 14))
              ),
              Container(
                  margin: const EdgeInsets.only(top: 4.0),
                  child:Text(exercise.nextExercise.title.toString(),
                      style: const TextStyle(
                          color: Color.fromRGBO(0, 19, 44, 1),
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 18))
              ),

            ]
        )
    );
  }
}