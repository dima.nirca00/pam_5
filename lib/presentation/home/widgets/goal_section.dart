import 'package:flutter/material.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../domain/homeModelsApi/goals.dart';


class GoalSection extends StatefulWidget {
  RxList<Goals> goals;

  GoalSection({super.key, required this.goals});

  @override
  _GoalSection createState() => _GoalSection(this.goals);
}

class _GoalSection extends State<GoalSection> {
  RxList<Goals> goals;

  _GoalSection(this.goals);

  int formatTime(int seconds) {
    return Duration(seconds: seconds).inMinutes;
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          //apply padding to all four sides
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(top: 12),
                  alignment: Alignment.centerLeft,
                  child: const Text("Start New Goal",
                      style: TextStyle(
                          color: Color.fromRGBO(0, 19, 44, 1),
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 18)),
                ),
                Container(
                  alignment: Alignment.centerRight,
                  child: const Text("See all",
                      style: TextStyle(
                          color: Color.fromRGBO(25, 143, 179, 1),
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 14)),
                ),
              ])),
      SizedBox(
        height: 290.0,
        child: ListView.builder(
          physics: const ClampingScrollPhysics(),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: goals.isEmpty ? 0 : goals.length,
          itemBuilder: (context, index) => Container(
              margin: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 15.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(24),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.1),
                    spreadRadius: 5,
                    blurRadius: 18,
                    offset: const Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              width: 310,
              height: 276,
              child: Stack(children: <Widget>[
                Container(
                  height: 144,
                  width: 310,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(goals[index].image.toString())),
                    borderRadius: const BorderRadius.all(Radius.circular(24.0)),
                    color: Colors.redAccent,
                  ),
                ),
                Positioned(
                  left: 246,
                  top: 124,
                  child: Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(24),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 18,
                          offset: const Offset(0, 3),
                        ),
                      ],
                      border: Border.all(
                          color: const Color.fromRGBO(239, 239, 239, 1), width: 2),
                    ),
                    child: SvgPicture.asset('resources/svg/start.svg',
                            height: 25, width: 25, fit: BoxFit.scaleDown)),
                ),
                Positioned(
                    left: 12,
                    top: 144,
                    child: Container(
                      alignment: Alignment.centerLeft,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 15.0, vertical: 5.0),
                      child: Text(goals[index].title.toString(),
                          style: const TextStyle(
                              color: Color.fromRGBO(0, 19, 44, 1),
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              fontSize: 24)),
                    )),
                Positioned(
                    left: 12,
                    top: 185,
                    child: Container(
                      alignment: Alignment.centerLeft,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 15.0, vertical: 0),
                      child: Text(goals[index].description.toString(),
                          style: const TextStyle(
                              color: Color.fromRGBO(148, 158, 159, 1),
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              fontSize: 14)),
                    )),
                Positioned(
                    left: 12,
                    top: 210,
                    child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        alignment: Alignment.centerLeft,
                        margin: const  EdgeInsets.symmetric(horizontal: 15.0, vertical: 0),
                        child: Row(
                          children: [
                            Container(
                              padding: const EdgeInsets.symmetric(horizontal: 8.0),
                              height: 32,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16),
                                color: const Color.fromRGBO(242, 255, 237, 1),
                                border: Border.all(
                                    color: const Color.fromRGBO(51, 188, 10, 1),
                                    width: 2),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  SvgPicture.asset('resources/svg/clock.svg'),
                                  Text(' ' + formatTime(goals[index].duration).toString() + ' min',
                                      style: const TextStyle(
                                          color: Color.fromRGBO(51, 188, 10, 1),
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14))
                                ],
                              ),
                            ),
                            const Padding(
                              padding: EdgeInsets.symmetric(horizontal: 8.0),
                            ),
                            Container(
                              padding: const EdgeInsets.symmetric(horizontal: 8.0),
                              height: 32,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16),
                                color: const Color.fromRGBO(255, 250, 238, 1),
                                border: Border.all(
                                    color: const Color.fromRGBO(255, 182, 5, 1),
                                    width: 2),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  SvgPicture.asset('resources/svg/calories.svg'),
                                  Text(' ' + goals[index].burnCalories.toString() + ' cal',
                                      style: const TextStyle(
                                          color: Color.fromRGBO(255, 182, 5, 1),
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14))
                                ],
                              ),
                            )
                          ],
                        )))
              ])),
        ),
      ),
    ]);
  }
}
