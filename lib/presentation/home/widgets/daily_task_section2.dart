import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lab4fitness/presentation/single_exercise_page/single_exercise2.dart';
import '../../../data/controllers/single_exercise_controller.dart';
import '../../../domain/homeModelsApi/daily_exercises.dart';
import 'package:http/http.dart' as http;

import '../../../domain/homeModelsApi/exercises_response.dart';

class DailyTaskSection2 extends StatefulWidget {
  RxList<DailyExercises> exercises;
  int numberPage;

  DailyTaskSection2({super.key, required this.exercises, required this.numberPage});

  @override
  _DailyTaskSection createState() => _DailyTaskSection(this.exercises, this.numberPage);
}

class _DailyTaskSection extends State<DailyTaskSection2> {
  RxList<DailyExercises> exercises;
  int numberPage;
  _DailyTaskSection(this.exercises, this.numberPage);
  bool hasMore = true;
  final controller = ScrollController();
  final controllerExercise = Get.put(ExerciseController());
  int formatTime(int seconds) {
    return Duration(seconds: seconds).inMinutes;
  }


  @override
  void initState(){
    super.initState();
    controller.addListener(() {
      if(controller.position.maxScrollExtent == controller.offset){
        fetch();
      }
    });
  }
  Future fetch() async{
    numberPage = numberPage + 1;
    RxList<DailyExercises> exercisesPaged  = RxList();
    final response = await http.get(Uri.parse('https://fitness-app-api.k8s.devebs.net/workout/exercise?page=' + numberPage.toString() + '&size=7'));
    if (response.statusCode == 200) {
      var map = await jsonDecode(response.body);
      var mapResponse = ExercisesResponse.fromJson(map);
      exercisesPaged.value = mapResponse.exercises
          .map((e) => DailyExercises(
        id: e.id,
        title: e.title,
        image: e.image,
        duration: e.duration,
        repetitions: e.repetitions,
        burnCalories: e.burnCalories,
      ))
          .toList();

        if(exercisesPaged.length < 7){
          hasMore = false;
        }
        setState((){
          exercises.addAll(exercisesPaged);
        });
    }

  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
            padding: const EdgeInsets.all(15),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerLeft,
                    child: const Text("Daily Task",
                        style: TextStyle(
                            color: Color.fromRGBO(0, 19, 44, 1),
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.bold,
                            fontSize: 18)),
                  ),
                  Container(
                    alignment: Alignment.centerRight,
                    child: const Text("See all",
                        style: TextStyle(
                            color: Color.fromRGBO(25, 143, 179, 1),
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.bold,
                            fontSize: 14)),
                  ),
                ])),
        SizedBox(
          height: 340.0,
          child: ListView.builder(
            padding: EdgeInsets.zero,
            controller: controller,
            shrinkWrap: true,
            itemCount: exercises.length + 1,
            itemBuilder: (ctx, index) {
              if(index < exercises.length){
                return GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => (
                              ExerciseSection2(numberEx: index+1, total: exercises.length)
                          ),
                        ),
                      );
                    },
                    child: Container(
                        margin: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 6.0),
                        // height: 48,
                        child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    height: 48,
                                    width: 48,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: NetworkImage(exercises[index].image.toString())),
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(8.0)),
                                      color: Colors.redAccent,
                                    ),
                                    //Image.network('https://img-19.commentcamarche.net/cI8qqj-finfDcmx6jMK6Vr-krEw=/1500x/smart/b829396acc244fd484c5ddcdcb2b08f3/ccmcms-commentcamarche/20494859.jpg')
                                  ),

                                  Container(
                                      height: 48,
                                      child:
                                      Stack(children: <Widget>[
                                        Positioned(
                                            left: 16,
                                            top: 0,
                                            child:
                                            Container(
                                              margin: const EdgeInsets.all(0),
                                              child: Text(
                                                  exercises[index]
                                                      .title.toString(),
                                                  style: const TextStyle(
                                                      color: Color.fromRGBO(0, 19, 44, 1),
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.bold,
                                                      fontSize: 16)),
                                            )),

                                        Container(
                                            margin: const EdgeInsets.only(
                                                top: 25.0), child:
                                        Row(
                                          children: [
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(15, 0, 0, 0),
                                              height: 21,
                                              child: Row(
                                                //mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  SvgPicture.asset('resources/svg/clock.svg'),
                                                  Text(' ' + formatTime(exercises[index].duration).toString() + ' min',
                                                      style: const TextStyle(
                                                          color: Color.fromRGBO(51, 188, 10, 1),
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight
                                                              .bold,
                                                          fontSize: 14))
                                                ],
                                              ),
                                            ),
                                            // const Padding(
                                            // padding: EdgeInsets.symmetric(horizontal: 8.0),
                                            // ),
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                              height: 21,
                                              child: Row(
                                                mainAxisAlignment:
                                                MainAxisAlignment.center,
                                                children: <Widget>[
                                                  SvgPicture.asset('resources/svg/calories.svg'),
                                                  Text(' ' + exercises[index].burnCalories.toString() + ' cal',
                                                      style: const TextStyle(
                                                          color: Color.fromRGBO(255, 182, 5, 1),
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight.bold,
                                                          fontSize: 14))
                                                ],
                                              ),
                                            )
                                          ],
                                        ))
                                      ]
                                      ))
                                ],
                              ),
                              Container(
                                height: 32,
                                width: 32,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(24),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.1),
                                      spreadRadius: 5,
                                      blurRadius: 18,
                                      offset: Offset(0, 3),
                                      // changes position of shadow
                                    ),
                                  ],
                                  border: Border.all(
                                      color: const Color.fromRGBO(239, 239, 239, 1),
                                      width: 2),
                                ),
                                child: SvgPicture.asset('resources/svg/start.svg',
                                        height: 10,
                                        width: 10,
                                        fit: BoxFit.scaleDown)

                              )
                            ])
                    ));
              }else{
                  return hasMore ? const Padding(
                      padding: EdgeInsets.symmetric(vertical: 0),
                      child: Center(child: CircularProgressIndicator())
                  ) : const Padding(
                      padding: EdgeInsets.symmetric(vertical: 2),
                      child: Center(child: Text('No more data')));
              }
            },
          ),
        ),
      ],
    );
  }
}