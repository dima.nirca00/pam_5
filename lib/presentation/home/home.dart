import 'package:flutter/material.dart';
import 'package:lab4fitness/presentation/home/widgets/daily_task_section2.dart';
import 'package:lab4fitness/presentation/home/widgets/goal_section.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';

class HomePage extends StatefulWidget {
  HomePage({super.key});

  @override
  _HomePage createState() => _HomePage();
}

class _HomePage extends State<HomePage> {


  _HomePage();
  final homeController = Get.put(HomeController());

  @override
  void initState() {
    Get.lazyPut(() => HomeController());
    HomeController controllerGoals = Get.find();
    controllerGoals.readGoalsApiData();
    HomeController controllerExercise = Get.find();
    controllerExercise.readExerciseApiDataPaged(1, 7);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    HomeController controllerGoals = Get.find();
    HomeController controllerExercise = Get.find();


    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(0.0), // here the desired height
          child: AppBar()),
        body: Column(
          children: <Widget>[
                Obx( () => controllerGoals.goalsList.isNotEmpty
            ? GoalSection(goals: controllerGoals.goalsList)
             : const Center(child: CircularProgressIndicator())
            ),
            Obx( () => controllerExercise.exercisesList.isNotEmpty
                ? DailyTaskSection2(exercises: controllerExercise.exercisesList, numberPage: 1)
                : const Center(child: CircularProgressIndicator())
            )
          ],
        )
    );
  }
}